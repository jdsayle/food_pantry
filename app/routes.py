from flask import render_template, flash, redirect, url_for, request
from flask_login import current_user, login_user, login_required, logout_user
from werkzeug.urls import url_parse
from app import serve, db
from app.forms import LoginForm, RegistrationForm, EditProfileForm
from app.models import User
from datetime import datetime

@serve.before_request
def before_request():
    if current_user.is_authenticated:
        current_user.last_login = datetime.utcnow()
        db.session.commit()

@serve.route('/')
@serve.route('/index')

@login_required
def index():
    ### Temporary user/post data, hardcoded into route logic,
    ### will be removed in the future
    posts = [
        {
            'author': {'username': 'J.D.'},
            'body': 'This is a post!'
        },
        {
            'author': {'username': 'Kena'},
            'body': 'I like to be awesome!'
        }
    ]
    return render_template("index.html", title="Food Pantry", posts=posts)

@serve.route('/login', methods=['GET', 'POST'])
def login():
    ### Next two lines will redirect logged in users to
    ### the home page if they try to otherwise access
    ### '/login'; VERY useful!
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        # logic from werkzeug.urls; used for security; stops CSRF-type stuff
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('index')
        return redirect(next_page)
    return render_template('login.html', title='Sign In', form=form)

@serve.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))

@serve.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a registered user!')
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)

@serve.route('/user/<username>')
@login_required
def user(username):
    user = User.query.filter_by(username=username).first_or_404()
    posts = [
        {'author': user, 'body': 'Test entry #1'},
        {'author': user, 'body': 'Test entry #2'}
    ]
    return render_template('user.html', user=user, posts=posts)

@serve.route('/edit_profile', methods=['GET', 'POST'])
@login_required
def edit_profile():
    form = EditProfileForm()
    if form.validate_on_submit():
        current_user.username = form.username.data
        current_user.short_bio = form.short_bio.data
        db.session.commit()
        flash('Your changes have been saved.')
        return redirect(url_for('edit_profile'))
    elif request.method == 'GET':
        form.username.data = current_user.username
        form.short_bio.data = current_user.short_bio
    return render_template('edit_profile.html', title='Edit Profile', form=form)
