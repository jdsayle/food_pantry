from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager
from config import Config

serve = Flask(__name__)
serve.config.from_object(Config)
db = SQLAlchemy(serve)
migrate = Migrate(serve, db)
login = LoginManager(serve)
login.login_view = 'login'

# at the bottom to stop circular imports
from app import routes, models
