from app import serve, db
from app.models import User, Post

@serve.shell_context_processor
def make_shell_context():
    return {'db': db, 'User': User, 'Post': Post}
